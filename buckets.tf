/* resource "aws_s3_bucket" "ailves_analytics-ie-raw-transactions" {
  count  = var.legacy.enable ? 1 : 0
  bucket = local.raw_transactions_bucket
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.dataservices[0].arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  tags = {
    Name = local.raw_transactions_bucket
  }

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "BucketAccess",
            "Effect": "Allow",
            "Principal": {
                "AWS" "arn:aws:iam::/AWSGlueServiceRole-Base"
            },
            "Action": [
                "s3:ListBucket",
                "s3:GetObject",
                "s3:GetBucketLocation",
                "s3:GetEncriptionConfiguration"
            ],
            "Resource": [
                "srn:aws:s3:::${local.raw_transactions_bucket}",
                "srn:aws:s3:::${local.raw_transactions_bucket}/*"
            ]
        }
    ]
}
POLICY

}
*/
resource "aws_s3_bucket" "test_s3" {
  bucket = local.raw_transactions_bucket
  acl    = "private"
  # Отправка уведомления об изменении этого бакета в другой бакет
  logging {
    target_bucket = aws_s3_bucket.log_s3.id
    target_prefix = "logs/"
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        #kms_master_key_id = aws_kms_key.dataservices[0].arn
        sse_algorithm     = "aws:kms"
      }
    }
  }
}

# Уровень доступа к этому бакету
resource "aws_s3_bucket_public_access_block" "test_s3_block" {
  bucket = aws_s3_bucket.test_s3.id

  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls  = true
  # restrict_public_buckets = true
}

# Отправка уведомления об изменении этого бакета в очередь
resource "aws_s3_bucket_notification" "bucket_notification" {
  count  = var.sns.enable ? 1 : 0
  bucket = aws_s3_bucket.test_s3.id

  topic {
    topic_arn     = aws_sns_topic.user_updates[count.index].arn
    events        = ["s3:ObjectCreated:*"]
    filter_suffix = ".log"
  }
}

# бакет для получения логов из test_s3
resource "aws_s3_bucket" "log_s3" {
  bucket = local.log_bucket
  acl    = "log-delivery-write"

  /*  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.dataservices[0].arn
        sse_algorithm     = "aws:kms"
      }
    } */
}