variable "aws_region1" {
  default = "us-east-2"
}

variable "aws_region2" {
  default = "us-east-1"
}

variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleInstance"
}

variable "sqs" {
  type = object({
    name = string
    #version = string
    #instance_size = string
    #replicas = number
    #nodes_per_az = number
    enable = bool
  })
  default = ({
    enable = false
    name   = "SQS Service"
    #version = string
    #instance_size = string
    #replicas = number
    #nodes_per_az = number
  })
}

variable "sns" {
  type = object({
    name = string
    #version = string
    #instance_size = string
    #replicas = number
    #nodes_per_az = number
    enable = bool
  })
  default = ({
    enable = false
    name   = "SNS Service"
    #version = string
    #instance_size = string
    #replicas = number
    #nodes_per_az = number
  })
}

variable "ec2" {
  type = object({
    name = string
    #version = string
    #instance_size = string
    #replicas = number
    #nodes_per_az = number
    enable = bool
  })
  default = ({
    enable = false
    name   = "SNS Service"
    #version = string
    #instance_size = string
    #replicas = number
    #nodes_per_az = number
  })
}

variable "web" {
  type = object({
    enable = bool
    name   = string
  })
  default = ({
    enable = true
    name   = "WEB Service"
  })
}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.1.0.0/16"
}

variable "public_subnet_cidr_blocks" {
  description = "Available cidr blocks for public subnets."
  type        = list(string)
  default = [
    "10.1.1.0/24",
    "10.1.2.0/24",
    "10.1.3.0/24",
    "10.1.4.0/24",
    "10.1.5.0/24",
    "10.1.6.0/24",
    "10.1.7.0/24",
    "10.1.8.0/24",
  ]
}

variable "private_subnet_cidr_blocks" {
  description = "Available cidr blocks for private subnets."
  type        = list(string)
  default = [
    "10.1.101.0/24",
    "10.1.102.0/24",
    "10.1.103.0/24",
    "10.1.104.0/24",
    "10.1.105.0/24",
    "10.1.106.0/24",
    "10.1.107.0/24",
    "10.1.108.0/24",
  ]
}

variable "public_subnet_count" {
  description = "Number of public subnets."
  type        = number
  default     = 2
}

variable "private_subnet_count" {
  description = "Number of private subnets."
  type        = number
  default     = 2
}

variable "enable_vpn_gateway" {
  description = "Enable a VPN gateway in your VPC."
  type        = bool
  default     = false
}

variable "kms_enable_key_rotation" {
  default = "true"
}

