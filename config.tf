terraform {
  required_version = "~> 0.15.0"

  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    /* aws = {
      version = "3.42.0"
    }
    random = {
      version = "3.1.0"
    } */
  }
  backend "s3" {
    region  = "us-east-2"
    bucket  = "ailves2009-terraform-state"
    key     = "dev/aws-sandbox.tfstate"
    acl     = "bucket-owner-full-control"
    encript = "true"
    #   kms_key_id = "arn:aws:kms:us-east-2:121850521501:alias/terraform"
    #   role_arn = "arn:.."
  }
}
