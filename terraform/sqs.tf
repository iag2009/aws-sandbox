resource "aws_sqs_queue" "terraform_queue" {
  name                      = "my-sqs1"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.terraform_queue_deadletter.arn
    maxReceiveCount     = 4
  })

  tags = {
    Service     = "demo"
    Environment = "dev"
  }
}

resource "aws_sqs_queue" "terraform_queue_deadletter" {
  name                      = "my-sqs1-deadletter"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10

  tags = {
    Service     = "demo"
    Environment = "dev"
  }
}

module "aws-sqs-module" {
  source = "terraform-aws-modules/sqs/aws"
  name   = "my-sqs-module"

  tags = {
    Service     = "demo"
    Environment = "dev"
  }
}
