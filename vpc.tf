/*module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.64.0"

  cidr = var.vpc_cidr_block

  azs             = data.aws_availability_zones.available.names
  private_subnets = slice(var.private_subnet_cidr_blocks, 0, var.private_subnet_count)
  public_subnets  = slice(var.public_subnet_cidr_blocks, 0, var.public_subnet_count)


  enable_nat_gateway = true
  enable_vpn_gateway = var.enable_vpn_gateway
}
*/

resource "aws_vpc" "foo" {
  cidr_block           = "10.6.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "Prod"
  }
}

resource "aws_vpc" "bar" {
  provider             = aws.alternative
  cidr_block           = "10.7.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "Backup"
  }
}

resource "aws_subnet" "prod_subnet_1" {
  vpc_id            = aws_vpc.foo.id
  availability_zone = data.aws_availability_zones.available.names[0]
  cidr_block        = "10.6.1.0/24"
  tags = {
    Name   = "prod_subnet_1 in ${data.aws_availability_zones.available.names[0]}"
    Region = "prod_subnet Region ${data.aws_region.current.description}"
  }
}

resource "aws_subnet" "prod_subnet_2" {
  vpc_id            = aws_vpc.foo.id
  availability_zone = data.aws_availability_zones.available.names[0]
  cidr_block        = "10.6.2.0/24"
  tags = {
    Name   = "prod_subnet_2 in ${data.aws_availability_zones.available.names[0]}"
    Region = "prod_subnet Region ${data.aws_region.current.description}"
  }
}