resource "aws_sns_topic" "user_updates" {
  count                     = var.sns.enable ? 1 : 0
  name = "user-updates-topic"
  # kms_master_key_id = "alias/aws/sns"
}