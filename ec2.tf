resource "aws_instance" "sqs" {
  count                  = var.ec2.enable ? 1 : 0
  ami           = var.aws_ami
  instance_type = "t2.micro"
  key_name      = "ailves2009aws"
  #security_groups = ["sg-840a7cf7"]
  vpc_security_group_ids = [aws_security_group.sqs[count.index].id]
  #ser_data = file("user_data.sh") #Static file
  user_data = templatefile("user_data.sh.tpl", {
    f_name = "Alex",
    l_name = "Ilves",
    names  = ["Vasja", "Kolja", "John", "Donald", "Masha"]
  })
  lifecycle {
    #prevent_destroy = true
    ignore_changes        = [ami]
    create_before_destroy = true
  }
  tags = {
    Name = var.instance_name
  }
}

resource "aws_security_group" "sqs" {
  count       = var.ec2.enable ? 1 : 0
  name        = "${var.aws_region1}-sqs-ec2"
  description = "sqs-ec2-Sec-Group"
}

resource "aws_security_group_rule" "sqs" {
  count             = var.ec2.enable ? 1 : 0
  type      = "ingress"
  from_port = 0
  to_port   = 0
  protocol  = "tcp"
  cidr_blocks = [
    var.vpc_cidr_block
  ]
  security_group_id = aws_security_group.sqs[count.index].id
}