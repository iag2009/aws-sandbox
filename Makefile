SHELL = /bin/bash
ACCOUNT := $(shell cat inputs.auto.tfvars.json | jq -r .account_id)
REGION := $(shell cat inputs.auto.tfvars.json | jq -r .region)
CLUSTER_GROUP := $(shell cat inputs.auto.tfvars.json | jq -r .cluster_group)
CLUSTER_NAME := $(shell cat inputs.auto.tfvars.json | jq -r .cluster_name)

.PHONY: init
init:
	rm -rf .terraform
	terraform init -backend-config=workspace_key_prefix="${ACCOUNT}/${REGION}/${CLUSTER_GROUP}"
	terraform workspace select "${CLUSTER_NAME}" || terraform workspace new "${CLUSTER_NAME}"

.PHONY: terraform
terraform: init
	terraform ${ACTION}

apply:
	ACTION=$@ $(MAKE) terraform

destroy:
	ACTION=$@ $(MAKE) terraform
