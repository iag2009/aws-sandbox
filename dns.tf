resource "aws_route53_zone" "main" {
  name = "ailves.com"
}

resource "aws_route53_zone" "sub" {
  name = "sub.ailves.com"
  vpc {
    vpc_id = aws_vpc.foo.id
  }
}

resource "aws_route53_vpc_association_authorization" "foo" {
  vpc_id  = aws_vpc.bar.id
  zone_id = aws_route53_zone.sub.id
}

resource "aws_route53_zone_association" "foo" {
  provider = aws.alternative
  vpc_id   = aws_route53_vpc_association_authorization.foo.vpc_id
  zone_id  = aws_route53_vpc_association_authorization.foo.zone_id
}

resource "aws_route53_record" "dev-ns" {
  zone_id = aws_route53_zone.main.zone_id
  name    = "sub.ailves.com"
  type    = "NS"
  ttl     = "30"
  records = aws_route53_zone.sub.name_servers
}

resource "aws_route53_record" "host1" {
  zone_id = aws_route53_zone.sub.zone_id
  name    = "host1.sub.ailves.com"
  type    = "A"
  ttl     = "30"
  records = [aws_instance.sqs[count.index].public_ip]
  count   = var.sqs.enable ? 1 : 0
}
