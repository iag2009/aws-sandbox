#!/bin/bash
#sudo yum -y update
sudo yum -y install httpd
myip='curl http://169.254.169.254/latest/meta-data/local-ipv4'
sudo touch /var/www/html/index.html
sudo chmod 606 /var/www/html/index.html

cat <<EOF > /var/www/html/index.html
<html>
<h2>Build by Terraform <font color="red"> v0.15</font></h2><br>
Owner ${f_name} ${l_name}<br>
<font color="green">Server PrivateIP: <font color="aqua">$myip<br><br>

%{ for x in names ~}
Hello to ${x} from ${f_name}<br>
%{ endfor ~}

</html>
EOF

sudo service httpd start
sudo chkconfig httpd on
