provider "aws" {
  profile = "default"
  #version = "3.6.0"
  region = var.aws_region1 #us-east-2
  #region = data.aws_region.current.name #us-east-2
}

provider "aws" {
  alias  = "alternative"
  region = var.aws_region2 #us-east-1
}

