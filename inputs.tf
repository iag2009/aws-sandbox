variable "aws_ami" {
  description = "Depends on location"
  type        = string
  default     = "ami-0ba62214afa52bec7"
}

variable "region" {
  type    = string
  default = "us-east-2"
}

#variable "tags" {
#    type = object({
#        BillingID = string
#        Owner = string
#    })
#}

variable "account_id" {
  type = string
}

variable "availability_zones" {
  type = list(string)
}

variable "cidr_block" {
  type = string
}

variable "cluster_group" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "multi_region" {
  type = object({
    enable        = bool
    backup_region = string
    active_region = string
  })
  /* default = ({
        enable = true
        backup_region = "us-east-1"
        active_region = "us-east-2"
    })*/
}

variable "legacy" {
  type = object({
    enable                   = bool
    gateway_pepper_reference = string
    /*  rds = object({
      identifier = string
      key_reference = string
    })
    elasticcache = object({
      identifier = string
      key_reference = string
    })
    elasticsearch = object({
      identifier = string
    })
    kms = object({
      dataservices = string
    }) */
    s3 = object({
      raw_transactions = string
    })
  })
}

variable "terraform_role_arn" {
  type = string
}