output "data-aws_availability_zones-names" {
  value = data.aws_availability_zones.available.names
}
output "data-aws_caller_identity-account_id" {
  value = data.aws_caller_identity.current.account_id
}
output "data-aws_region-name" {
  value = data.aws_region.current.name
}

output "data-aws_region-description" {
  value = data.aws_region.current.description
}
/**** aws_ami *****************/
output "data-aws_ami-latest_ubuntu_ami_id" {
  value = data.aws_ami.latest_ubuntu_ami.id
}
output "data-aws_ami-latest_ubuntu_ami-name" {
  value = data.aws_ami.latest_ubuntu_ami.name
}

/**** VPC and Subnets ********/
output "data-aws_vpcs-ids" {
  value = data.aws_vpcs.my_vpcs.ids
}/*
output "data-aws_vpc-prod_vpc-id" {
  value = data.aws_vpc.prod_vpc.id
}
output "data-aws_vpc-prod_vpc-cidr_block" {
  value = data.aws_vpc.prod_vpc.cidr_block
}
/* output "aws_subnet-prod_subnet_1" {
  value = aws_subnet.prod_subnet_1.cidr_block
}
output "aws_subnet-prod_subnet_2" {
  value = aws_subnet.prod_subnet_2.cidr_block
}*/


/**** Route53 **************/
output "public-sub_dns-name" {
  description = "Public DNS names of the load balancer for this project"
  value       = aws_route53_zone.sub.name
}

/**** VPC ****************/
output "aws_vpc-foo-owner" {
  value = aws_vpc.foo.owner_id
}
output "aws_vpc-foo-cidr_block" {
  value = aws_vpc.foo.cidr_block
}
output "aws_vpc-bar-cidr_block" {
  value = aws_vpc.bar.cidr_block
}
output "aws_vpc-foo-arn" {
  value = aws_vpc.foo.arn
}
output "aws_vpc-foo-id" {
  value = aws_vpc.foo.id
}
output "aws_vpc-foo-instance_tenancy" {
  value = aws_vpc.foo.instance_tenancy
}
output "aws_vpc-foo-enable_dns_support" {
  value = aws_vpc.foo.enable_dns_support
}
output "aws_vpc-foo-enable_dns_hostnames" {
  value = aws_vpc.foo.enable_dns_hostnames
}
output "aws_vpc-foo-tags_all" {
  value = aws_vpc.foo.tags_all
}
