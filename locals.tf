locals {
  #gitlab_endpoint_service_name = local.gitlab_endpoint_services[var.region]

  /*gitlab_endpoint_services = {
        us-east-2 = "com.amazonaws.us-east-2.git-codecommit"
    }
    */

  #artifactory_endpoint_service_name = local.artifactory_endpoint_services[var.region]

  /*artifactory_endpoint_services = {
        us-east-2 = "com.amazonaws.us-east-2.git-codecommit"
    }
    */
  raw_transactions_bucket = "${var.cluster_group}-${var.region}-dataservices-transactions"
  log_bucket              = "${var.cluster_group}-${var.region}-logging"
}