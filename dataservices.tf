resource "aws_kms_key" "dataservices" {
  count                   = var.legacy.enable ? 0 : 1
  description             = "dataservices=${var.cluster_group}-${var.region}"
  deletion_window_in_days = 7
  enable_key_rotation     = var.kms_enable_key_rotation
  policy = <<POLICY
    {
    "Version": "2012-10-17",
    "Id": "key-default-1",
    "Statement": [
        {
            "Sid": "Enable IAM User Permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.account_id}:root"
            },
            "Action": "kms:*",
            "Resource": "*"
        }
    ]
}
POLICY
}